# one_click_qlik

----

`Guillaume Charbonnier, Capgemini 2018`

----

# Introduction

`One clik qlik` is a template repository for Qlik Sense and Qlik View projects.

It should be used with [cookiecutter](https://github.com/audreyr/cookiecutter).

> Note: You can find more information regarding installation on the [official documentation](https://cookiecutter.readthedocs.io/en/latest/installation.html)


## Quick start

Once you have installed `cookiecutter`, you can create your first projet with only one command line (and an internet connection):

```
cookiecutter https://gitlab.com/gu.charbon/one_click_qlik.git
```


## Structure of the templated repository

The generated repository will look like the following:

```
|__ {{ repository_name }}
  |
  |__ 1.Application/
  |__ 2.QVD/
  |__ 3.Include/
  |__ 4.Mart/
  |__ 5.Config/
  |__ 6.Script/
  |__ 7.Export/
  |__ 8.Import/
  |__ 9.Misc/
  |
  |__ CHANGELOG.txt
  |__ README.md
  |__ version.txt
```

The `README.md` file will be templated with some values like repository name and
author but there is no much templating done other thant that.


## How does the templating work ?


You can use `{{ cookiecutter.VARIABLE_NAME }}` anywhere in the repository:

- In file names
- In directory names
- In file content

The string will be replaced by the value of `VARIABLE_NAME` given either in:

- the value declared in `cookiecutter.json` file is `--no-input` is used

- the chosen value when prompted is no option is specified

The templating engine used is Jinja2. You can use a lot of filters and expressions to do some complex templating. The documentation is available [here](http://jinja.pocoo.org/docs/2.10/)
