# {{ cookiecutter.repository_name }}

`{{ cookiecutter.author}}, Capgemini 2018`

-----

## Introduction

> General presentation of {{ cookiecutter.repository_name }}


## Quick start

> Some info on how to spin up an instance of the application on desktop Qlik


## Installation on Server

> Some info on how to install the project on a server instance of Qlik

## Deploying in production

> Some prerequisites before deploying the application

## Contact the author

You can reach the author by mail at {{ cookiecutter.author_mail }}.
